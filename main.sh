#!/bin/bash

mkdir ${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh} -p
if [ -f ./list.html ]
then
	export FILE_LIST=./list.html
elif [ -f /usr/share/jepordy.sh/list.html ]
then
	export FILE_LIST=/usr/share/jepordy.sh/list.html
else
	echo 'Unable to find `list.html`. Exiting.'
	exit -1
fi

if [ -f ./question.html ]
then
	export FILE_QUESTION=./question.html
elif [ -f /usr/share/jepordy.sh/question.html ]
then
    export FILE_QUESTION=/usr/share/jepordy.sh/question.html
else
	echo 'Unable to find `question.html`. Exiting.'
	exit -1
fi

if [ -f ./score.txt ]
then
	export FILE_SCORE=./score.txt
elif [ -f ${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/score.txt} ] 
then
	export FILE_SCORE=${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/score.txt} 
elif [ -f /usr/share/jepordy.sh/score.txt ]
then	
	cp /usr/share/jepordy.sh/score.txt ${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/score.txt}
	chmod 644 ${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/score.txt} 
	export FILE_SCORE=${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt} 
else
	echo 'Unable to find `score.txt`. Creating in current directory.'
	exit -1
fi

if [ -f ./questions.txt ]
then
	export FILE_QUESTIONS=./questions.txt
elif [ -f ${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt} ]
then
	export FILE_QUESTIONS=${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt} 
elif [ -f /etc/jepordy.sh/questions.txt ]
then
	cp /etc/jepordy.sh/questions.txt ${XG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt}
	chmod 644 ${XG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt}
	export FILE_QUESTIONS=${XDG_CONFIG_HOME:-${HOME/.config}/jepordy.sh/questions.txt}
else
	echo 'Unable to find `questions.txt'. Exiting.
	exit -1
fi

if [ -f ./server.sh ]
then
	export FILE_SERVER=./server.sh
elif [ -f /usr/share/jepordy.sh/server.sh ]
then
	export FILE_SERVER=/usr/share/jepordy.sh/server.sh
else
	echo 'Unable to find `server.sh'. Exiting.
	exit -1
fi

if [ -f ./static/style.css ]
then
	export FILE_STYLE=./static/style.css
elif [ -f /usr/share/jepordy.sh/static/style.css ]
then
	export FILE_STYLE=/usr/share/jepordy.sh/static/style.css
else
	echo 'Unable to find `sytle.css'. Exiting.
	exit -1
fi

echo "Listening on http://0.0.0.0:2001"
socat TCP-LISTEN:2001,fork,reuseaddr EXEC:${FILE_SERVER}
