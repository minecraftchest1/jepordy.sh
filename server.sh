#!/bin/bash

read HTTP_REQUEST
HTTP_URL=$(echo ${HTTP_REQUEST} | cut -d ' ' -f 2)


# SETUP ENVIROMENT VARS USED IN HTML

	export CAT1=$(cat $FILE_QUESTIONS | cut -d '-' -f 1 | uniq | sed '1q;d')
	export CAT2=$(cat $FILE_QUESTIONS | cut -d '-' -f 1 | uniq | sed '2q;d')
	export CAT3=$(cat $FILE_QUESTIONS | cut -d '-' -f 1 | uniq | sed '3q;d')
	export CAT4=$(cat $FILE_QUESTIONS | cut -d '-' -f 1 | uniq | sed '4q;d')
 	export CAT5=$(cat $FILE_QUESTIONS | cut -d '-' -f 1 | uniq | sed '5q;d')
	export TEAM1=$(grep "team1" $FILE_SCORE | cut -d '-' -f 2)
	export TEAM2=$(grep "team2" $FILE_SCORE | cut -d '-' -f 2)
	export CAT=$(echo "${HTTP_URL}" | cut -d '/' -f 3)
	export QUESTION_ID=$(echo "${HTTP_URL}" | cut -d '/' -f 4)
	export CATEGORY=$(cat ${FILE_QUESTIONS} | cut -d '-' -f 1 | uniq | sed "${CAT}q;d")
	export QUESTION_TEXT=$(grep "${CATEGORY}" questions.txt | grep "${QUESTION_ID}" | cut -d '-' -f 3)

if [[ ${HTTP_URL} == '/' ]]
then
	echo 'HTTP/1.1 200 OK'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/html'
	echo ''

	cat $FILE_LIST | envsubst

elif [[ ${HTTP_URL} == "/cat/"* ]]
then
	echo 'HTTP/1.1 200 OK'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/html'
	echo ''

	cat $FILE_QUESTION | envsubst

elif [[ ${HTTP_URL} == "/team/1"* ]]
then
	echo 'HTTP/1.1 303 NOT MODIFIED'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/html'
	echo 'Location: /'
	echo ''

	echo "team1   - $((TEAM1+QUESTION_ID))" > $FILE_SCORE
	echo "team2   - ${TEAM2}" >> $FILE_SCORE

elif [[ ${HTTP_URL} == "/team/2"* ]]
then
	echo 'HTTP/1.1 303 NOT MODIFIED'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/html'
	echo 'Location: /'
	echo ''

	echo "team1   - ${TEAM1}" > $FILE_SCORE
	echo "team2   - $((TEAM2+QUESTION_ID))" >> $FILE_SCORE

elif [[ ${HTTP_URL} == "/reset"* ]]
then
	echo 'HTTP/1.1 303 NOT MODIFIED'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/html'
	echo 'Location: /'
	echo ''

	echo "team1   - 0" > $FILE_SCORE
	echo "team2   - 0" >> $FILE_SCORE

elif [[ ${HTTP_URL} == '/static/style.css' ]]
then
	echo 'HTTP/1.1 200 OK'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/css'
	echo ''

	cat $FILE_STYLE
else
	echo 'HTTP/1.1 404 NOT FOUND'
	echo "Date: echp $(date '+%a, %d %b %Y %H %M %S GMT')"
	echo 'Server: Jepordy.sh 0.1.0'
	echo 'Content-Type: text/plain'
	echo ''

	echo 'The requested URI was not found.'
fi

